package com.company.chain_of_responsibility;

public class Police implements Handler {

    private Handler next;

    @Override
    public void setNext(Handler handler) {
        this.next =handler;
    }

    @Override
    public void isNessesary(TypesOfAccidents typesOfAccidents) {
        if(typesOfAccidents == TypesOfAccidents.FIRE){
            next.isNessesary(typesOfAccidents);
        }else {
            System.out.println("Policja zadysponowana");
            next.isNessesary(typesOfAccidents);
        }

    }
}
