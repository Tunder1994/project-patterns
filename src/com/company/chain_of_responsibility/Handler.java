package com.company.chain_of_responsibility;

public interface Handler {

    void setNext(Handler handler);
    void isNessesary(TypesOfAccidents typesOfAccidents);
}
