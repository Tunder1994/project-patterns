package com.company.chain_of_responsibility;

public class FireStation implements Handler {

    private Handler next;

    @Override
    public void setNext(Handler handler) {
        this.next =handler;
    }

    @Override
    public void isNessesary(TypesOfAccidents typesOfAccidents) {
        if(typesOfAccidents == TypesOfAccidents.TEFT || typesOfAccidents == TypesOfAccidents.BEAT){
            next.isNessesary(typesOfAccidents);
        }else {
            System.out.println("Straż zadysponowana");
            next.isNessesary(typesOfAccidents);
        }

    }
}
