package com.company.chain_of_responsibility;

public class Client {

    public static void main(String[] args) {
        TypesOfAccidents typesOfAccidents;
        Handler handler = new Police();

        Handler police = new Police();
        Handler emergency = new EmergencyMedicalServices();
        Handler fireDepartment = new FireStation();

        fireDepartment.setNext(emergency);
        police.setNext(fireDepartment);
        police.isNessesary(TypesOfAccidents.ACCIDENT);
    }
}
