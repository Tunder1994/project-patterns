package com.company.chain_of_responsibility;

public enum TypesOfAccidents {
    FIRE,
    BEAT,
    TEFT,
    ACCIDENT;
}
