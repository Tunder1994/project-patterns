package com.company.proxy;

public class SmsProxy implements SmsGateway {

    SmsGatewayImpl gateway = new SmsGatewayImpl();

    @Override
    public void sendMessage(String message, String phoneNumber) {
        if(phoneNumber.length()<9){
            throw new IllegalArgumentException("Invalid lenght of number");
        }else{
            if(message.length()<120){
                throw new IllegalArgumentException("Invalid length of message");
            }else {
                gateway.sendMessage(message, phoneNumber);
                System.out.println();
                System.out.println("Message send.");
            }
        }
    }


}
