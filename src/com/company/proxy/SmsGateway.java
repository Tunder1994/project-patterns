package com.company.proxy;

public interface SmsGateway {
    void sendMessage(String message, String phoneNumber);
}
