package com.company.proxy;

public class Client {

    public static void main(String[] args) {
        SmsProxy proxy = new SmsProxy();

        proxy.sendMessage("93iroij43ojr439jr4398ntu4trtbg3498tn43fn09rjneoknfoweindif0ingoisfng0wrngokwng024ingtkngrkn0in430ng340ing4ni0n43g0in403i4ngi043ngi034ng0i34ng430ing3i40ng430gn", "123456789");
        proxy.sendMessage("93iroij43ojr439jr4398ntu4trtbg3498tn43fn09rjneoknfoweindif0ingoisfng0wrngokwng024ingtkngrkn0in430ng340ing4ni0n43g0in403i4ngi043ngi034ng0i34ng430ing3i40ng430gn", "145673");

    }
}
