package com.company.proxy;

public class SmsGatewayImpl implements SmsGateway {
    @Override
    public void sendMessage(String message, String phoneNumber) {
        System.out.printf("Wysłano: %s na numer %s",message,phoneNumber);

    }
}
