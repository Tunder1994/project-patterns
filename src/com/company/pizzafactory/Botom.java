package com.company.pizzafactory;

public enum Botom {
    THIN("Thin Bottom"),
    THICK("Thick Bottom"),
    WHOLEMEAL("Wholemeal Bottom");

    private String type;

    Botom(String type) {
        this.type = type;
    }
}
