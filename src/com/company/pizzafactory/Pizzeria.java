package com.company.pizzafactory;

public class Pizzeria{


    public Pizza recipe(Botom bottom) {

        switch (bottom){
            case THIN:
                return new ThinBotom();
            case THICK:
                return new ThickBotom();
            case WHOLEMEAL:
                return new WholemealBotom();
        }
        throw new RuntimeException("There is no this pizza");
    }
}
