package com.company.pizzafactory;

public interface Pizza {
    String recipe();
}
