package com.company.strategy;

public class CityStrategy implements EngineStrategy {

    @Override
    public void operation() {
        System.out.println("CITY");
    }
}
