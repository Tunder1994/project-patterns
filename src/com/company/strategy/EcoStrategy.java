package com.company.strategy;

public class EcoStrategy implements EngineStrategy {

    @Override
    public void operation() {
        System.out.println("ECO");
    }
}
