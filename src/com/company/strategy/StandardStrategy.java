package com.company.strategy;

public class StandardStrategy implements EngineStrategy {

    @Override
    public void operation() {
        System.out.println("STANDARD");
    }
}
