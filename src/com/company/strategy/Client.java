package com.company.strategy;

import java.util.Collection;

public class Client {

    public static void main(String[] args) {
        Context context = new Context();

        context.chooser(StategyType.SPORT).operation();
    }
}
