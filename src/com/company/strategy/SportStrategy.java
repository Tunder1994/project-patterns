package com.company.strategy;

public class SportStrategy implements EngineStrategy {

    @Override
    public void operation() {
        System.out.println("SPORT");
    }
}
