package com.company.strategy;

public class Context {
    EngineStrategy engineStrategy;


    EngineStrategy chooser (StategyType stategyType){
        switch (stategyType){
            case CITY: return new CityStrategy();
            case ECO: return new EcoStrategy();
            case SPORT: return new SportStrategy();
            case STANDARD: return new StandardStrategy();
        }
        return null;
    }

}
