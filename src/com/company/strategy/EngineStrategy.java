package com.company.strategy;

public interface EngineStrategy {

    void operation();
}
