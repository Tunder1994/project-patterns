package com.company.strategy;

public enum StategyType {
    CITY,
    ECO,
    SPORT,
    STANDARD;
}
