package com.company.databasesingleton;

public class User {
    private String firstName;
    private String surname;
    private String email;
    private String phoneNumber;

    public User(String firstName, String surname, String email, String phoneNumber) {
        this.firstName = firstName;
        this.surname = surname;
        this.email = email;
        this.phoneNumber = phoneNumber;
    }


}
