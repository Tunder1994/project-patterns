package com.company.databasesingleton;

import java.util.ArrayList;
import java.util.List;

public class UserDataBaseSingleton {

    private static UserDataBaseSingleton singleton;
    private List<User> userList;

    private UserDataBaseSingleton() {
       userList = new ArrayList<>();
    }

    public static UserDataBaseSingleton getInstance(){
        if(singleton== null){
            singleton = new UserDataBaseSingleton();
        }
        return singleton;
    }
}
