package com.company.command;

public class Subtraction implements Calculate {
    private int a;
    private int b;

    public Subtraction(int a, int b) {
        this.a = a;
        this.b = b;
    }

    @Override
    public void execute() {
        System.out.println(a-b);
    }
}
