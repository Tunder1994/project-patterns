package com.company.command;

public class Client {
    public static void main(String[] args) {
        Invoker invoker = new Invoker();
        invoker.setCalculateList(new Multiplication(3,4));
        invoker.setCalculateList(new Multiplication(6,4));
        invoker.setCalculateList(new Multiplication(3,7));
        invoker.setCalculateList(new Multiplication(3,1));
        invoker.setCalculateList(new Multiplication(3,44));
        invoker.setCalculateList(new Multiplication(3,42));
        invoker.invoke();
    }
}
