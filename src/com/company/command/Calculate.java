package com.company.command;

public interface Calculate {

    void execute();
}
