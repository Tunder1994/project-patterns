package com.company.command;

import java.util.ArrayList;
import java.util.List;

public class Invoker {
    private List<Calculate> calculateList = new ArrayList<>();

    public void setCalculateList(Calculate calculate){
        calculateList.add(calculate);
    }

    public void invoke(){
        calculateList.forEach(Calculate::execute);
    }
}
