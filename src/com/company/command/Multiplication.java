package com.company.command;

public class Multiplication implements Calculate {
    private int a;
    private int b;

    public Multiplication(int a, int b) {
        this.a = a;
        this.b = b;
    }

    @Override
    public void execute() {
        System.out.println(a*b);;
    }
}
