package com.company.bulider;

// Bulidera uzywamy kiedy chcemy zaimplementowac jakis obiekt a nie wszystkie pola sa wymagane


public class User {
    private String userName;
    private String email;
    private String password;
    private int age;
    private String firstName;
    private String surname;
    private String phoneNumber;

    public static class UserBulider {
        private String userName;
        private String email;
        private String password;
        private int age;
        private String firstName;
        private String surname;
        private String phoneNumber;

        public UserBulider(String userName, String email, String password) {
            this.userName = userName;
            this.email = email;
            this.password = password;
        }

        public UserBulider age(int age){
            this.age = age;
            return this;
        }

        public UserBulider firstName(String firstName){
            this.firstName = firstName;
            return this;
        }

        public UserBulider surname(String surname){
            this.surname = surname;
            return this;
        }

        public UserBulider phoneNumber(String phoneNumber){
            this.phoneNumber = phoneNumber;
            return this;
        }

        public User bulid(){
            User user = new User();

            user.userName = userName;
            user.email = email;
            user.password = password;
            user.firstName = firstName;
            user.surname = surname;
            user.age = age;
            user.phoneNumber = phoneNumber;
            return user;
        }

    }



    @Override
    public String toString() {
        return "User{" +
                "userName='" + userName + '\'' +
                ", email='" + email + '\'' +
                ", password='" + password + '\'' +
                ", age=" + age +
                ", firstName='" + firstName + '\'' +
                ", surname='" + surname + '\'' +
                ", phoneNumber='" + phoneNumber + '\'' +
                '}';
    }



}


