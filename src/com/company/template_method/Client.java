package com.company.template_method;

public class Client {

    public static void main(String[] args) {
        SocialNetwork fb = new Facebook("dawid", "password");

        fb.logIn();
        fb.sendMessage("Message");
        fb.logOff("dawid");
    }
}
