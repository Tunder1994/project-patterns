package com.company.template_method;

public class Facebook extends SocialNetwork{

    public Facebook(String userName, String password) {
        super(userName, password);
    }


    @Override
    boolean logIn() {
        System.out.println("Zalogowano");
        return true;
    }

    @Override
    boolean sendData(String message) {
        System.out.println("Wysłano");
        return true;
    }

    @Override
    boolean logOff(String userName) {
        System.out.println("Wylogowano");
        return true;
    }
}
