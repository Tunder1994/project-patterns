package com.company.template_method;

public abstract class SocialNetwork {

    private String userName;
    private String password;

    public SocialNetwork(String userName, String password) {
        this.userName = userName;
        this.password = password;
    }

    public boolean sendMessage(String message) {
        if(userName!=null && password!=null){
            System.out.println("Wiadomość wysłano");
            return true;
        }
        return false;
    }

    abstract boolean logIn();
    abstract boolean sendData(String message);
    abstract boolean logOff(String userName);
}
