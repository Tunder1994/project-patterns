package com.company.flyweight;

import java.util.HashMap;
import java.util.LinkedHashMap;

public class WordFactory {

    Word word;
    HashMap<String, Word> map = new LinkedHashMap<>();

    String getWord(String getKey, Language language){
        Word word = map.get(getKey);
        if(word== null){
            switch (language){
                case POLISH:{
                    map.put(getKey, new GetPolishWord(getKey, false));
                    return map.get(getKey).getWord();
                }
                case ENGLISH:{
                    map.put(getKey, new GetEnglishWord(getKey, false));
                    return map.get(getKey).getWord();
                }
            }
        }
        return map.get(getKey).getWord();
    }

}
