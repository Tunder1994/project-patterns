package com.company.flyweight;

public class Client {

    public static void main(String[] args) {
        WordFactory wordFactory = new WordFactory();
        Word word = new GetPolishWord("Dawid", false);

        System.out.println(wordFactory.getWord("Dawid", Language.POLISH));
    }
}
