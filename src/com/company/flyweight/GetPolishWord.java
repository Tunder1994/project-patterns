package com.company.flyweight;

public class GetPolishWord implements Word {
    private String word;
    private boolean isVulgar;

    public GetPolishWord(String word, boolean isVulgar) {
        this.word = word;
        this.isVulgar = isVulgar;
    }

    public boolean isVulgar() {
        return isVulgar;
    }

    public String word(){
        return word;
    }

    @Override
    public String getWord() {
        if (isVulgar == true){
            return word + " is vulgar";
        }else {
            return  word + " isn't vulgar";
        }
    }
}
