package com.company.flyweight;

public enum Language {
    POLISH,
    ENGLISH;
}
