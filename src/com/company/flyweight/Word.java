package com.company.flyweight;

public interface Word {
    String getWord();
}
