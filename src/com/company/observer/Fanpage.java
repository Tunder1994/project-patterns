package com.company.observer;

public interface Fanpage {


    void addPost(String content);

    void nofifyObservers(String newContent);

    void attachObserver(Observer observer);

}
