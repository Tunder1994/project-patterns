package com.company.observer;

public class Client {

    public static void main(String[] args) {
        Fanpage fanpage = new SnarkyPuppyFanpage();
        fanpage.attachObserver(new Phone());
        fanpage.attachObserver(new Email());
        fanpage.addPost("Koncet");
        fanpage.addPost("Koncet1");
        fanpage.addPost("Koncet2");
        fanpage.addPost("Koncet3");
        fanpage.addPost("Koncet4");
        fanpage.addPost("Koncet5");


    }
}
