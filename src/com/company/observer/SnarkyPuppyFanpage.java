package com.company.observer;

import java.util.ArrayList;
import java.util.List;

public class SnarkyPuppyFanpage implements Fanpage {

    private List<Observer> observers = new ArrayList<>();
    private List<String> posts = new ArrayList<>();


    @Override
    public void addPost(String content) {
        posts.add(content);
        nofifyObservers(content);
    }

    @Override
    public void nofifyObservers(String newContent) {
        observers.forEach(observer -> observer.update(newContent));
    }

    @Override
    public void attachObserver(Observer observer) {
        observers.add(observer);
    }
}
