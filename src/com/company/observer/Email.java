package com.company.observer;

public class Email implements Observer {
    @Override
    public void update(String content) {
        System.out.println();
        System.out.println("Nowe post!");
        System.out.println(content);
        System.out.println();
    }
}
