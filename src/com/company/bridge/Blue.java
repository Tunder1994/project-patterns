package com.company.bridge;

public class Blue implements Colour {


    @Override
    public String getColour() {
        return "Blue";
    }
}
