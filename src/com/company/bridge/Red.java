package com.company.bridge;

public class Red implements Colour {
    @Override
    public String getColour() {
        return "Red";
    }
}
