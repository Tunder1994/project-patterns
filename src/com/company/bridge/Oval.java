package com.company.bridge;

public class Oval extends Shape {

    public Oval(Colour colour) {
        super(colour);
    }

    @Override
    String getColoredShape() {
        return "Oval " + colour.getColour();
    }
}
