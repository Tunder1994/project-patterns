package com.company.bridge;

public interface Colour {
    String getColour();
}
