package com.company.bridge;

public abstract class Shape {

    Colour colour;

    public Shape(Colour colour) {
        this.colour = colour;
    }

    abstract String getColoredShape();
}
