package com.company.bridge;

public class Ractange extends Shape {
    public Ractange(Colour colour) {
        super(colour);
    }

    @Override
    String getColoredShape() {
        return "Ractangle " + colour.getColour();
    }
}
