package com.company.adapter;

public class Adapter {

    Product product;

    public String getName(){
        return product.getName();
    }

    public double getPrice(double rate){
        return product.getPrice() * rate;
    }

    public Product getProduct(double rate){
        String name = product.getName();
        double price = product.getPrice() *rate;

        return new Product(price, name);
    }
}
