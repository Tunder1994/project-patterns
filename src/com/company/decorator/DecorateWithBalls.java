package com.company.decorator;

public class DecorateWithBalls extends ChristmassTreeDecorator{

    public DecorateWithBalls(ChristmassTree christmassTree) {
        super(christmassTree);
    }

    @Override
    public void decorate() {
        super.decorate();
        System.out.print( " and a lot of christmass balls");
    }
}
