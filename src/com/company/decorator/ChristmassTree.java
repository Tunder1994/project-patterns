package com.company.decorator;

public interface ChristmassTree {
    void decorate();
}
