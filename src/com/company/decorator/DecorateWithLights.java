package com.company.decorator;

public class DecorateWithLights extends ChristmassTreeDecorator{

    public DecorateWithLights(ChristmassTree christmassTree) {
        super(christmassTree);
    }

    @Override
    public void decorate() {
        super.decorate();
        System.out.print(" with colorfull lights");
    }
}
