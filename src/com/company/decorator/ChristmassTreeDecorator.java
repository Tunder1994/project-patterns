package com.company.decorator;

public abstract class ChristmassTreeDecorator implements ChristmassTree {

    private ChristmassTree christmassTree;

    public ChristmassTreeDecorator(ChristmassTree christmassTree) {
        this.christmassTree = christmassTree;
    }

    @Override
    public void decorate() {
        christmassTree.decorate();
    }
}
