package com.company.decorator;

public class Client {
    public static void main(String[] args) {
        ChristmassTree christmassTree = new ChristmassTreeImpl();
        christmassTree.decorate();
        System.out.println();
        christmassTree = new DecorateWithLights(christmassTree);
        christmassTree.decorate();
        System.out.println();
        christmassTree = new DecorateWithBalls(christmassTree);
        christmassTree.decorate();
        System.out.println();

    }
}
